export interface IVideoGame {
    id: number
    name: string
    year?: number
    genre: string
    rate: number
}