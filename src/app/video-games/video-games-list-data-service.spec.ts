import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { AppComponent } from '../app.component';
import { AppModule } from '../app.module';
import { IVideoGame } from './data/IVideoGame';
import { VideoGamesListDataService } from './video-games-list-data-service';

describe('VideoGamesListDataService', () => {
  let service: VideoGamesListDataService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let videoGame: IVideoGame;
  
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [VideoGamesListDataService]
    })
    .compileComponents();
    
    service = TestBed.inject(VideoGamesListDataService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', inject([VideoGamesListDataService], (service: VideoGamesListDataService) => {
    expect(service).toBeTruthy();
  }));
});
