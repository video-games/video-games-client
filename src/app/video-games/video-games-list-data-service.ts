import {Directive, Injectable, PipeTransform} from '@angular/core';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {DecimalPipe} from '@angular/common';
import {debounceTime, delay, switchMap, tap} from 'rxjs/operators';
import { IVideoGame } from './data/IVideoGame';
import { HttpStatusCodes, VideoGamesActions, VideoGamesServiceBase } from '../shared/service-base';
import { HttpClient } from '@angular/common/http';
import { SortColumn, SortDirection } from '../directives/sortable.directive';

interface SearchResult {
  videoGames: IVideoGame[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: SortColumn;
  sortDirection: SortDirection;
}

const compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

function sort(videoGames: IVideoGame[], column: SortColumn, direction: string): IVideoGame[] {
  if (direction === '' || column === '') {
    return videoGames;
  } else {
    return [...videoGames].sort((a, b) => {
      const res = compare(a[column]!, b[column]!);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(videoGames: IVideoGame, term: string) {
  return videoGames.name.toLowerCase().includes(term.toLowerCase())
    || videoGames.genre?.toLowerCase().includes(term.toLowerCase())
    || videoGames.year && videoGames.year.toString().includes(term)
}

@Directive()
@Injectable()
export class VideoGamesListDataService extends VideoGamesServiceBase {
   
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _videoGames$ = new BehaviorSubject<IVideoGame[]>([]);
  private _total$ = new BehaviorSubject<number>(0);

  private _state: State = {
    page: 1,
    pageSize: 4,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };

  allVideoGames: IVideoGame[] = [];

  constructor(http: HttpClient) {
    super(http);

    //this.refresh()
  }

  public refresh() {
    let action = VideoGamesActions.VideoGameList;
    this.get<{status: number, data: IVideoGame[]}>(action).subscribe(result => {
        if(!!result && result.status == HttpStatusCodes.OK) {
            this.allVideoGames = result.data

            this._search$.pipe(
              tap(() => this._loading$.next(true)),
              debounceTime(200),
              switchMap(() => this._search()),
              delay(200),
              tap(() => this._loading$.next(false))
            ).subscribe(result => {
              this._videoGames$.next(result.videoGames);
              this._total$.next(result.total);
            });
        
            this._search$.next();
        }
    });
  }

  get videoGames$() { return this._videoGames$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); }
  set sortColumn(sortColumn: SortColumn) { this._set({sortColumn}); }
  set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  private _search(): Observable<SearchResult> {
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    // 1. sort
    let videoGames = sort(this.allVideoGames, sortColumn, sortDirection);
    console.log("sort", this.allVideoGames, sortColumn, sortDirection, videoGames)  
    
    // 2. filter
    videoGames = videoGames.filter(videoGame => matches(videoGame, searchTerm));
    const total = videoGames.length;

    // 3. paginate
    videoGames = videoGames.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({videoGames: videoGames, total});
  }
}
