import { HttpClient } from '@angular/common/http';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';
import { IVideoGame } from '../data/IVideoGame';
import { NgbdSortableHeader, SortEvent } from '../../directives/sortable.directive';
import { VideoGamesDataService } from '../video-games-data-service';
import { VideoGamesListDataService } from '../video-games-list-data-service';
import { RouterStates } from 'src/app/router/router-states';

@Component({
  selector: 'video-game-list',
  templateUrl: './video-game-list.component.html',
  styleUrls: ['./video-game-list.component.scss']
})
export class VideoGameListComponent implements OnInit {

  constructor(public service: VideoGamesListDataService) {
    this.videoGames$ = service.videoGames$;
    this.total$ = service.total$;    
  }

  RouterStates = RouterStates
  videoGames$: Observable<IVideoGame[]>;
  total$: Observable<number>;

  ngOnInit(): void {
    this.service.refresh();
  }
 
  @ViewChildren(NgbdSortableHeader) headers!: QueryList<NgbdSortableHeader>;
  onSort(event: any) {
console.log("onSort", event)    
    let column = event.column
    let direction = event.direction

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  onDelete() {
    this.service.refresh()
  }
}
