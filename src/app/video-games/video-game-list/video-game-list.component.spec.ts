import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VideoGamesListDataService } from '../video-games-list-data-service';

import { VideoGameListComponent } from './video-game-list.component';

describe('VideoGameListComponent', () => {
  let component: VideoGameListComponent;
  let fixture: ComponentFixture<VideoGameListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [VideoGamesListDataService],
      declarations: [ VideoGameListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoGameListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
