import {Component, EventEmitter, Input, OnInit, Output, Type} from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { HttpStatusCodes } from 'src/app/shared/service-base';
import { IVideoGame } from '../data/IVideoGame';
import { VideoGamesDataService } from '../video-games-data-service';

@Component({
  selector: 'video-game-delete',
  templateUrl: './video-game-delete.component.html',
  styleUrls: ['./video-game-delete.component.scss']
})
export class VideoGameDeleteComponent implements OnInit {
  constructor(private modalService: NgbModal, public service: VideoGamesDataService) { }

  @Input()
  videoGame!: IVideoGame

  @Output() 
  delete : EventEmitter<void> = new EventEmitter<void>();
  
  closeResult = '';

  ngOnInit(): void {
  }

  showConfirmation(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      if(result = 'save')
        this.deleteWithApi();
    }, (reason) => {
      //do not delete
      this.closeResult = `Dismissed`;
    });
  }

  deleteWithApi() {
    this.service.deleteVideoGame(this.videoGame.id).subscribe((result) => 
      {
        if(!!result && result.status == HttpStatusCodes.OK)
          this.delete.emit()
      })
  }

}
