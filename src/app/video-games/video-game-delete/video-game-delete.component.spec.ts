import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IVideoGame } from '../data/IVideoGame';
import { VideoGamesDataService } from '../video-games-data-service';

import { VideoGameDeleteComponent } from './video-game-delete.component';

describe('VideoGameDeleteComponent', () => {
  let component: VideoGameDeleteComponent;
  let fixture: ComponentFixture<VideoGameDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [VideoGamesDataService, NgbModal],
      declarations: [ VideoGameDeleteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoGameDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
