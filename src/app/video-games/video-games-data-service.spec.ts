import { TestBed } from '@angular/core/testing';
import { VideoGamesDataService } from './video-games-data-service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { IVideoGame } from './data/IVideoGame';
import { of } from 'rxjs';
import { IVideoGameResult } from '../shared/service-base';

describe('VideoGamesDataService', () => {
  let service: VideoGamesDataService;
  let valueServiceSpy: jasmine.SpyObj<VideoGamesDataService>;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let videoGame: IVideoGame;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpClient', ['getVideoGame']);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [VideoGamesDataService, { provide: HttpClient, useValue: spy }]
    });
//    httpClient = TestBed.inject(HttpClient);
    service = TestBed.inject(VideoGamesDataService);
//    httpMock = TestBed.inject(HttpTestingController);
    valueServiceSpy = TestBed.inject(VideoGamesDataService) as jasmine.SpyObj<VideoGamesDataService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  //TypeError: Cannot read property 'returnValue' of undefined

  // it('#getVideoGame should return stubbed value from a spy', () => {
  //   const stubValue = of(<IVideoGameResult>{status: 200, data: {id: 1, name: "Stub Name"}});
  //   valueServiceSpy.getVideoGame.and.returnValue(stubValue);
  
  //   expect(service.getVideoGame(1))
  //     .toBe(stubValue, 'service returned stub value');
  //   expect(valueServiceSpy.getVideoGame.calls.count())
  //     .toBe(1, 'spy method was called once');
  //   expect(valueServiceSpy.getVideoGame.calls.mostRecent().returnValue)
  //     .toBe(stubValue);
  // });  
});
