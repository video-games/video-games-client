import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { StateService, UIRouter, UIRouterModule } from '@uirouter/angular';
import { RouterInitialStates } from 'src/app/router/router-states';
import { uiRouterConfigFn } from 'src/app/router/router.config';
import { IVideoGame } from '../data/IVideoGame';
import { VideoGamesDataService } from '../video-games-data-service';

import { VideoGameEditComponent } from './video-game-edit.component';

describe('VideoGameEditComponent', () => {
  let component: VideoGameEditComponent;
  let fixture: ComponentFixture<VideoGameEditComponent>;
  let router: UIRouter;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, UIRouterModule.forRoot({ states: RouterInitialStates, config: uiRouterConfigFn, useHash: true })],
      providers: [VideoGamesDataService, StateService],
      declarations: [ VideoGameEditComponent ]
    })
    .compileComponents();

    router = TestBed.inject(UIRouter);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoGameEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  //Error: Can't resolve all parameters for StateService: (?).

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });

  // it('should have Edit in title', () => {
  //   fixture.componentInstance.videoGame=<IVideoGame>{ id: 1 };
  //   fixture.detectChanges();     
  //     expect(fixture.nativeElement.querySelector('h4.title').textContent).toContain('Edit');
  //   })

  // it('should have New in title', () => {
  //   fixture.componentInstance.videoGame=<IVideoGame>{ id: 0 };
  //   fixture.detectChanges();     
  //     expect(fixture.nativeElement.querySelector('h4.title').textContent).toContain('New');
  //   })
  
});
