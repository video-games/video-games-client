import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StateParams, StateService, TargetState, UIRouter } from '@uirouter/angular';
import { RouterStates } from 'src/app/router/router-states';
import { HttpStatusCodes } from 'src/app/shared/service-base';
import { IVideoGame } from '../data/IVideoGame';
import { VideoGamesDataService } from '../video-games-data-service';

@Component({
  selector: 'app-video-game-edit',
  templateUrl: './video-game-edit.component.html',
  styleUrls: ['./video-game-edit.component.scss']
})
export class VideoGameEditComponent implements OnInit {

  constructor(private service: VideoGamesDataService, private $state: StateService) { }

  videoGame!: IVideoGame
  formGroup!: FormGroup;

  ngOnInit(): void {
    this.loadVideoGame()
  }

  loadVideoGame() {
    let id = this.$state.params["id"]
    if(!!id) {
      this.service.getVideoGame(id).subscribe((result) => {
        if(!!result && result.status == HttpStatusCodes.OK)
          this.videoGame = result.data;
          this.setupValidation()
      })   
    }
    else {
      this.videoGame = <IVideoGame>{id: 0, rate: 1};
      this.setupValidation()
    }
  }

  setupValidation() {
    this.formGroup = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.maxLength(20)
      ]),
      genre: new FormControl('', [
        Validators.maxLength(20)
      ]),
      year: new FormControl('', [
        Validators.min(1980),
        Validators.max((new Date()).getFullYear()),
      ]),
      rate: new FormControl(this.videoGame.rate, [
        Validators.required
      ])
    }); 
  }

  onSubmit() {
    if(!this.videoGame.rate)
      this.videoGame.rate = 0

    var serviceCall = !!this.videoGame.id 
      ? this.service.editVideoGame(this.videoGame) 
      : this.service.addVideoGame(this.videoGame)

    serviceCall.subscribe((result) => {
      if(!!result && result.status == HttpStatusCodes.OK)
        this.returnToList();
    })   
  }

  onCancel() {
    this.returnToList();
  }

  returnToList() {
    this.$state.go(RouterStates.list);
  };

}
