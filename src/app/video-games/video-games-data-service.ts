import { Directive, Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { IVideoGame } from "./data/IVideoGame";
import { HttpClient } from "@angular/common/http";
import { IVideoGameResult, VideoGamesActions, VideoGamesServiceBase } from "../shared/service-base";

@Directive()
@Injectable()
export class VideoGamesDataService extends VideoGamesServiceBase {
    constructor(http: HttpClient) {
        super(http);
    }
    
    getVideoGame(id: number): Observable<IVideoGameResult> {
        let action = VideoGamesActions.GetVideoGame;
        return this.get<IVideoGameResult>(action, id)
    }

    addVideoGame(data: IVideoGame): Observable<IVideoGameResult> {
        let action = VideoGamesActions.AddVideoGame;
        return this.put(action, data);
    }

    editVideoGame(data: IVideoGame): Observable<IVideoGameResult> {
        let action = VideoGamesActions.UpdateVideoGame;
        return this.put(action, data)
    }

    deleteVideoGame(id: number): Observable<IVideoGameResult> {
        let action = VideoGamesActions.DeleteVideoGame;
        return this.delete(action, id);
    }  
}