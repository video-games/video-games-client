import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'year'
})
export class YearPipe implements PipeTransform {

  transform(value: number | undefined | null, ...args: unknown[]): string {
    return !!value ? value.toString() : "";
  }
}
