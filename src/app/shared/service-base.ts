import { HttpClient } from "@angular/common/http";
import { Directive, Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { IVideoGame } from "../video-games/data/IVideoGame";
import { UrlBuilder } from "./url-builder";

export enum Endpoints {
    ApiEndpoint = 'http://localhost:55296/api/videogames',
    MockEndpoint = 'mock-domain/api'
}

export enum VideoGamesActions {
    VideoGameList = "",
    GetVideoGame = "",
    AddVideoGame = "add",
    UpdateVideoGame = "update",
    DeleteVideoGame = "delete",
}

export enum HttpStatusCodes {
    OK = 200,
    Invalid = 400,
    Error = 500,
}

export interface IVideoGameResult {
    status: number,
    data: IVideoGame
}

@Directive()
@Injectable()
export class VideoGamesServiceBase {
    constructor(private http: HttpClient){}

    protected createUrl(action: string, isMockAPI: boolean = false): string {
        const urlBuilder: UrlBuilder = new UrlBuilder(
            isMockAPI ? Endpoints.MockEndpoint : Endpoints.ApiEndpoint,
            action
        );
        return urlBuilder.toString();
    }

    protected get<T>(action: string, id: number | null = null, isMockAPI: boolean = false): Observable<T>
    {
        let urlAction = !id 
            ? action
            : `${id.toString()}/${action}`

        let url = this.createUrl(urlAction, isMockAPI);

        return this.http.get(url)
            .pipe(map((r: any) =>
            {
                return this.checkResult(r);
            }), catchError(err => this.handleError(err, action)));
    }

    protected put(action: string, data: any, isMockAPI: boolean = false): Observable<IVideoGameResult>
    {
        let url = this.createUrl(action, isMockAPI);
        return this.http.put(url, data)
            .pipe(map((r: any) =>
            {
                return this.checkResult(r);
            }), catchError(err => this.handleError(err, action)));
    }

    protected delete(action: string, id: number | null = null, isMockAPI: boolean = false): Observable<IVideoGameResult>
    {
        let urlAction = !id 
            ? action
            : `${id.toString()}/${action}`

        let url = this.createUrl(urlAction, isMockAPI);

        return this.http.delete(url)
            .pipe(map((r: any) =>
            {
                return this.checkResult(r);
            }), catchError(err => this.handleError(err, action)));
    }

    protected handleError(err: any, operation = 'operation') {
        if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            alert(`Server Unexpected Error: ${err.error.message}`)
            console.error('An error occurred:', err.error.message);
          } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            alert(`Server Validation Error: ${err.error}`)
            console.error(`Server Validation Error: ${err.error}`);
          }

        return [];
    }

    private checkResult(result: any): any
    {
        return { data: result, status: HttpStatusCodes.OK};
    }
}