import { Transition } from "@uirouter/angular";
import { VideoGameEditComponent } from "../video-games/video-game-edit/video-game-edit.component";
import { VideoGameListComponent } from "../video-games/video-game-list/video-game-list.component";

export enum RouterStates {
  list = "list",
  add = "add",
  edit = "edit",
}

export enum RouteUrls {
  list = "list",
  add = "add",
  edit = "edit/:id",
}

export const RouterStateList = { name: RouterStates.list, url: RouteUrls.list, component: VideoGameListComponent };
export const RouterStateAdd = { name: RouterStates.add, url: RouteUrls.add, component: VideoGameEditComponent };
export const RouterStateEdit = { name: RouterStates.edit, url: RouteUrls.edit, component: VideoGameEditComponent };

export const RouterInitialStates = [RouterStateList, RouterStateEdit, RouterStateAdd]
