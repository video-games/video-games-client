import { Injector } from "@angular/core";
import { UIRouter } from "@uirouter/angular";
import { RouterStates } from "./router-states";

/** UIRouter Config Function  */
export function uiRouterConfigFn(router: UIRouter, injector: Injector) {
    // Configure the initial state
    // If the browser URL doesn't matches any state when the router starts,
    // go to the `hello` state by default
    router.urlService.rules.initial({ state: RouterStates.list });
}
  