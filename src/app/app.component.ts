import { Component } from '@angular/core';
import { RouterStates } from './router/router-states';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  RouterStates = RouterStates

  title = 'Video Games';
}
