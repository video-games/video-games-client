import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { VideoGameListComponent } from './video-games/video-game-list/video-game-list.component';
import { VideoGameEditComponent } from './video-games/video-game-edit/video-game-edit.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Transition, UIRouter, UIRouterModule } from "@uirouter/angular";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VideoGamesDataService } from './video-games/video-games-data-service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { VideoGamesListDataService } from './video-games/video-games-list-data-service';
import { DecimalPipe } from '@angular/common';
import { VideoGameDeleteComponent } from './video-games/video-game-delete/video-game-delete.component';
import { RouterInitialStates, RouterStateAdd, RouterStateEdit, RouterStateList } from './router/router-states';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { YearPipe } from './pipes/year.pipe';
import { NgbdSortableHeader } from './directives/sortable.directive';
import { uiRouterConfigFn } from './router/router.config';

@NgModule({
  declarations: [
    AppComponent,
    VideoGameListComponent,
    VideoGameEditComponent,
    VideoGameDeleteComponent,
    YearPipe,
    NgbdSortableHeader
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    NgbModule,
    UIRouterModule.forRoot({ states: RouterInitialStates, config: uiRouterConfigFn, useHash: true }),
    HttpClientModule,
    NgBootstrapFormValidationModule.forRoot(),
    NgBootstrapFormValidationModule
  ],
  providers: [
    VideoGamesDataService,
    VideoGamesListDataService,
    DecimalPipe,
    YearPipe,
    NgbdSortableHeader
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
